<head><style>html {height: 100%; line-height: 100%; color: red; font-weight: bold; text-align: center;}</style></head>
<div><?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$rand1 = sprintf("%05d", rand(1,99999));
$rand2 = sprintf("%05d", rand(1,99999));
$devid = $rand1. "-". $rand2. "-". basename($_FILES["fileToUpload"]["name"]);
$target_dir = "uploads/";
$target_file = $target_dir . $devid;
if (isset($_POST['img-desc']))
{
    $imgdesc = " - ". basename($_POST["img-desc"]);
} else {
    $imgdesc = "";
}
$fileType = 1;
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$spoiler = "";
$isSpoiler = $_POST['isSpoiler'];
if ($isSpoiler == "select") {
    $spoiler = "spoiler user";
}
else {
}
echo "<br>";
// Check file size
if ($_FILES["fileToUpload"]["size"] > 2000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
} 
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    include_once('getid3/getid3.php');
    $getID3 = new getID3;
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else {
        echo "Invalid or corrupted file. Only image and video files are allowed.";
        $uploadOk = 0;
    }
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "mp4" && $imageFileType != "wav" ) {
    echo "Only JPG, JPEG, PNG, GIF, MP4, and WAV files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		$output = "<div class=\"image $spoiler $oc\" date=\"".date('F jS\, Y')."\" img-desc=\"". htmlspecialchars($imgdesc) ."\" devid=\"$devid\"><img src=\"$target_file\"></div>";
		$file_data = $output. "\n";
		$file_data .= file_get_contents('images.php');
		file_put_contents('images.php', $file_data);
		header("location: index.php#home");
    } else {
        echo "";
    }
}
?>

 