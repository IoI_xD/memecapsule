<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
<br>
<form style="text-align: center; margin: 5px;" action="upload.php" runat="server" method="post" enctype="multipart/form-data" >
	<strong>Upload:</strong><br>
		<input type="file" name="fileToUpload" id="fileToUpload" onchange="loadFile(event)" required><br>
	<label>Image Description (optional):</label><br> <input type="input" id="img-desc" name="img-desc"></input><br>
		<input type="submit" value="Upload" name="submit onclick="return" confirm('Are you sure you are ready to submit?')"><br>
		<img style="display: block; margin: 0 auto; max-width: 300px; max-height: 300px;" id="output"/>
        <label for="isSpoiler" style="white-space: nowrap">
            Mark as spoiler: <input type="checkbox" name="isSpoiler" id="isSpoiler" value="select">
        </label>
		<br>
		<br>
	<strong>Using backslashes will cause problems with the final result. I'm working on disabling said characters, for now don't use them. Other special characters are fine.</strong>
	</form>

    <div id="images" class="images">
        <?php include('images.php'); ?> 
    </div>