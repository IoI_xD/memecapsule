var img = new Image;
img.src = 'images/hitmarker.png';

$(document).ready(function () {
    if (window.location.hash == "#home") {
        $('#main').load('main.php');
    }
    if (window.location.hash == "#about") {
        $('#main').load('about.php');
    }
    if (window.location.hash == "#report") {
        $('#main').load('report.php');
    }
    if (window.location.hash == "#contact") {
        $('#main').load('contact.php');
    }
    if (window.location.hash == "#userport") {
        $('#main').load('test.php');
    }
    if (window.location.hash == "#login") {
        $('#main').load('registration/login.php');
    }
}); 

document.addEventListener('mousedown', function(e) {
    e = e || window.event;
    var tag = document.createElement('img');
    console.log(e);
    tag.src = img.src;
    tag.style.position = 'absolute';
    tag.style.height = '32px';
    tag.style.width = '32px';
    tag.style.top = (e.pageY - 16 || e.clientY - 16) + 'px';
    tag.style.left = (e.pageX - 16 || e.clientX - 16) + 'px';
	tag.style.pointerEvents = "none";
	tag.style.zIndex = '3';
    this.body.appendChild(tag);
	setTimeout(function(){$(tag).remove();}, 50);
}, false);


	function home() {
		$('#main').load('main.php');
	}
	
	function about() {
		$('#main').load('about.php');
	}

	function report() {
		$('#main').load('report.php');
	}
	
	function contact() {
		$('#main').load('contact.php');
	}
	
	function test() {
		$('#main').load('test.php');
	}

	function login() {
		$('#main').load('registration/login.php');
	}

