<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" >
  <head>
	<title>MemeCapsule</title>
	<meta property="og:site_name" content="MemeCapsule">
	<meta property="og:type" content="memes.xd">
	<meta property="og:url" content="ioi-xd.net/memecapsule">
	<meta property="og:image" content="xD.png">
	<meta property="og:description" content="saddest site on the internet">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="vid-js.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/8.7.1/lazyload.min.js"></script>
	<script src="script.js"></script>
  </head>
  <body id="dev">
      <div class="content">
		<?php include('tabs.php'); ?>
		<div id="main">
        <?php include('main.php'); ?> 
		</div>
      </div>
  </body>
</html>